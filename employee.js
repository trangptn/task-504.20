import  { Person }  from "./person.js";
class Employee extends Person{
    __employer;
    __salary;
    __position;

    constructor(paramEmployer,paramSalary,paramPosition)
    {
        super(paramEmployer,paramSalary,paramPosition);
        this._personName;
        this._personAge;
        this._gender;
        this.__employer=paramEmployer;
        this.__salary=paramSalary;
        this.__position=paramPosition;
    }

    getEmployeInfo()
    {
        return this.__employer +" - " +this.__salary+" - "+this.__position;
    }

    getSalary()
    {
        return this.__salary;
    }

    setSalary(paramSalary)
    {
        this.__salary=paramSalary;
    }

    getPosition()
    {
        return this.__position;
    }

    setPosition(paramPosition)
    {
        this.__position=paramPosition;
    }
}

export {Employee}