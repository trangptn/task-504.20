import  { Person }  from "./person.js";
import { Employee } from "./employee.js";
var person1=new Person("Jonh",30,"Nam");

console.log(person1);
console.log(person1 instanceof Person);
console.log(person1 instanceof Employee);

var employee1= new Employee("silver",1000000,"Default");

console.log(employee1);
console.log(employee1.getEmployeInfo());
employee1.setSalary(3000000);
console.log(employee1.getSalary());
employee1.setPosition("Relative");
console.log(employee1.getPosition());
console.log(employee1 instanceof Employee);
console.log(employee1 instanceof Person);
