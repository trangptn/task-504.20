class Person{
    _personName;
    _personAge;
    _gender;
    constructor(paramName, paramAge,paramGender)
    {
        this._personName=paramName;
        this._personAge=paramAge;
        this._gender=paramGender;
    }

    getPersonInfo()
    {
        return this._personName + "-" + this._personAge +"-" +this._gender;
    }
}
export{ Person };